from smbus import SMBus


class GY906Manager:
    SMBUS_ADDRESS = 0x01
    I2C_ADDRESS = 0x5a
    DATA_ADDRESS = 0x07
    
    def __init__(self):
        self.i2c_bus = None

    def initialize(self):
        self.i2c_bus = SMBus(GY906Manager.SMBUS_ADDRESS)

    def read_data(self):
        data = self.i2c_bus.read_word_data(GY906Manager.I2C_ADDRESS, 
                                           GY906Manager.DATA_ADDRESS)

        # Error Handling
        if (data >> 15) == 1:
            return 0

        # Convert F to C
        return (data * 0.02) - 273.15
