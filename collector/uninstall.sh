#!/bin/bash

DIRECTORY="/opt/genecafe-logger"
SERVICE_FILE="genecafe-logger.service"

if (( $EUID != 0 )); then
	echo "This script requires the root authentication."
	exit
fi

# Service Unregistration
systemctl disable ${SERVICE_FILE}
systemctl daemon-reload

# Remove Files
rm -rf ${DIRECTORY} /etc/systemd/system/${SERVICE_FILE}
