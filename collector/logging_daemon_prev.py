import os, sys
from datetime import datetime

from ir_data_collector import IRCollector


if __name__ == '__main__':
    # First Fork
    pid = os.fork()

    if pid > 0:
        # Parent Dies
        exit(0)
    else:
        # Change Working Directory
        os.chdir('/')
        os.setsid()
        os.umask(0)
        
        # Second Fork
        pid = os.fork()

        if pid > 0:
            # Parent Dies
            exit(0)
        else:
            # Daemon Process
            sys.stdout.flush()
            sys.stderr.flush()
            
            si = open(os.devnull, 'r')
            so = open(os.devnull, 'a+')
            se = open(os.devnull, 'a+')

            os.dup2(si.fileno(), sys.stdin.fileno())
            os.dup2(so.fileno(), sys.stdout.fileno())
            os.dup2(se.fileno(), sys.stderr.fileno())

            # Process Begin
            collector = IRCollector()
            collector.initialize()
            collector.routine()

            # Process End
            exit(0)

