#!/bin/bash

DIRECTORY="/opt/genecafe-logger"
SERVICE_FILE="genecafe-logger.service"

if (( $EUID != 0 )); then
	echo "This script requires the root authentication."
	exit
fi

# File Installation
mkdir -p ${DIRECTORY}
cp gy906_manager.py server_database.py logging_daemon.py ir_data_collector.py ${SERVICE_FILE} ${DIRECTORY}
ln -s ${DIRECTORY}/${SERVICE_FILE} /etc/systemd/system/${SERVICE_FILE}

# Service Registration
systemctl daemon-reload
# systemctl enable ${SERVICE_FILE}  # Not used yet.
