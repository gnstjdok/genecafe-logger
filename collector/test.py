from ir_data_collector import IRCollector


# Test code for running the program.
DEBUG_FLAG = False

collector = IRCollector()
collector.initialize()
collector.routine(debug=DEBUG_FLAG)
