import time
import signal
import threading

from gy906_manager import GY906Manager
from server_database import InfluxDBManager


class IRCollector:
    COLLECT_PERIOD = 0.25  # Second
    AUTH_FILE = '/etc/genecafe/auth/influx.auth'
    DATA_NAME = 'test'

    def __init__(self):
        self.gy906_manager = GY906Manager()
        self._db = None
        self._cleanup = False

        # Signal Handler
        signal.signal(signal.SIGINT, self.cleanup)
        signal.signal(signal.SIGTERM, self.cleanup)

    def initialize(self):
        # Setup DB
        self._db = InfluxDBManager()
        self._db.connect(account_path=IRCollector.AUTH_FILE)

        # Setup I2C
        self.gy906_manager.initialize()

    def cleanup(self, *args):
        self._db.close()
        self._cleanup = True

        # Exit here because the DB has been already closed gracefully.
        exit(0)

    def timer_function(self, debug: bool = False):
        # Time record
        begin_time_ns = int(time.time() * 1000)

        try:
            # Read data
            value = self.gy906_manager.read_data()

            # Send to DB
            self._db.insert_raw_data(data=value,
                                     name=IRCollector.DATA_NAME,
                                     debug=debug)
        finally:
            # Set next timer
            elapsed_time = int(time.time() * 1000) - begin_time_ns
            next_work_time = max(0, IRCollector.COLLECT_PERIOD - (elapsed_time)/1000.0)

            # If not got a SIGTERM, start next timer
            if not self._cleanup:
                threading.Timer(next_work_time, self.timer_function, [debug]).start()


    def routine(self, debug: bool = False):
        # Timer start
        self.timer_function(debug=debug)

        # Infinite loop until getting a SIGTERM
        while not self._cleanup:
           pass

