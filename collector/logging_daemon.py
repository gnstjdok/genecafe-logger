from ir_data_collector import IRCollector


if __name__ == '__main__':
    # Process Begin
    collector = IRCollector()
    collector.initialize()
    collector.routine()
