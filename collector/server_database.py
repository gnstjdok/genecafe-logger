import os
import datetime

from influxdb import InfluxDBClient


class InfluxDBManager:
    def __init__(self):
        self.host = None
        self.pw = None
        self.user = None
        self.db = None
        self.port = None
        self.client = None
    
    def connect(self, account_path: str) -> None:
        # Read MySQL account
        if not os.path.exists(account_path):
            print('Account file does not exists.')
            exit(1);

        with open(account_path, 'r') as file:
            lines = file.readlines()
            for line in lines:
                line = line[:-1]

                # Error Handling
                if len(line) == 0: continue
                if line[0] == '#': continue

                # Parse Host
                if line[:5] == 'host=':
                    self.host = line[5:]

                # Parse Port
                if line[:5] == 'port=':
                    self.port = line[5:]

                # Parse Database
                if line[:9] == 'database=':
                    self.db = line[9:]

                # Parse User
                if line[:5] == 'user=':
                    self.user = line[5:]

                # Parse Password
                if line[:9] == 'password=':
                    self.pw = line[9:]

        # Load Client
        self.client = InfluxDBClient(host=self.host, 
                                    port=self.port,
                                    username=self.user, 
                                    password=self.pw, 
                                    database=self.db)


    def close(self) -> None:
        self.client.close()

    def insert_raw_data(self, data: float, name: str, debug: bool = False) -> bool:
        record = {
                    "measurement": f"genecafe_{name}",
                    "tags": {
                        "host": "Sunny InfluxDB",
                        "region": "Asia/Seoul"
                        },
                    "time": datetime.datetime.utcnow(),
                    "fields": {
                        "IR_value": data
                        }
                    }

        if debug:
            print(record)
        result = self.client.write_points([record,])
        return result

